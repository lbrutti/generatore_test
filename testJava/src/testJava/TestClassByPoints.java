package testJava;

public class TestClassByPoints {
	private MovingObject2Dim mo = null;
	
	public static void main(String[] args) {
		TestClassByPoints self = new TestClassByPoints();
		
		
		Position2Dims position = new Position2Dims();
		Position2Dims startingPoint = new Position2Dims();
		Position2Dims arrivalPoint  = new Position2Dims();
		

		
		startingPoint.setX(0);
		startingPoint.setY(0);
		
		arrivalPoint.setX(10);
		arrivalPoint.setY(10);
		
		
		self.mo = new MovingObject2Dim("mov1", startingPoint, arrivalPoint);
		self.mo.setCurrentTick(1);
		
		
		// loop fino al ttl dell'oggetto
		int ttl = 100;
		while (self.mo.getCurrentTick()<ttl ){
			System.out.println(self.mo.applyRule());
			self.mo.setCurrentTick(self.mo.getCurrentTick()+1);
		}
		
	}

}
