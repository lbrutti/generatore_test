package testJava;

import abstractClasses.Position;

public class Position2Dims extends Position{
	
	public double getX() {
		return super.getCoord("x");
	}
	public void setX(double x) {
		super.setCoord("x", x);
	}
	public double getY() {
		return super.getCoord("y");
	}
	public void setY(double y) {
		super.setCoord("y", y);
	}
	

}
