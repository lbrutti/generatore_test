package testJava;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.math.plot.Plot2DPanel;

import abstractClasses.Position;

public class TestClassByAngle {
	
	public static void main(String[] args) {
		TestClassByAngle self = new TestClassByAngle();
		List<MovingObject2Dim> movingObjects = new ArrayList<MovingObject2Dim>();
		int objectsNum = 2;
		int meetingTime = 50;
		Double[] thetas = new Double[2];
		PrintWriter csvWriter = null;
		thetas[0]= 45.0;
		thetas[1] = 60.0;
		
		while (objectsNum>0){
			movingObjects.add(new MovingObject2Dim("", null, null));
			objectsNum--;
		}
		
		// primo moving object e' libero:
		MovingObject2Dim freeObject = movingObjects.get(0);
		Position2Dims freeObjectStartingPoint = new Position2Dims();
		Position2Dims freeObjectSrrivalPoint  = new Position2Dims();
		
		freeObjectStartingPoint.setX(1);
		freeObjectStartingPoint.setY(2);
		
		freeObjectSrrivalPoint.setX(0);
		freeObjectSrrivalPoint.setY(0);
		
		freeObject.setStartingPoint(freeObjectStartingPoint);
		freeObject.setArrivalPoint(freeObjectSrrivalPoint);
		freeObject.setID("FreeObject");
		
		freeObject.setTheta(thetas[0]);
		freeObject.setCurrentTick(meetingTime);
		freeObject.setTtl(100);
		
		//compute meetPoint_star: used to compute second moving object parametric equation:
		// this is the point where meeting should occour if no avoidance is act
		Position meetPoint_star = freeObject.applyRule();
		//workaround... fare funzione a parte
		freeObject.setCurrentTick(1);
		System.out.println("Position meetPoint_star : " + meetPoint_star);
		
		
		// creare secondo moving object tale che abbia un traiettoria passante per meetPoint_star:
		MovingObject2Dim slaveObject = movingObjects.get(1);
		Position2Dims slaveObjectStartingPoint = new Position2Dims();
		slaveObjectStartingPoint.setX(1);
		slaveObjectStartingPoint.setY(2);
		slaveObject.setCurrentTick(meetingTime);
		slaveObject.setStartingPoint(meetPoint_star);
		slaveObject.setArrivalPoint(meetPoint_star);
		slaveObject.setID("SlaveObject");
		slaveObject.setTheta(thetas[1]);
		slaveObject.setTtl(100);

		for(MovingObject2Dim m : movingObjects){	
			String CSV = "";
			m.setCurrentTick(0);
			// loop fino al ttl dell'oggetto
			while (m.getCurrentTick()<m.getTtl() ){
				CSV += m.applyRule().toCSV();
				m.setCurrentTick(m.getCurrentTick()+1);
			}
			try {
				csvWriter = new PrintWriter("csv/"+m.getID()+".txt");
				csvWriter.print(CSV);
				csvWriter.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Double[] solutions = slaveObject.computeAvoidanceThick(freeObject, 0);
		System.out.println("solutions[0] = " + solutions[0]);
		System.out.println("solutions[1] = " + solutions[1]);
		
	}

}
