package testJava;

import java.util.function.Function;

import abstractClasses.MovingObject;
import abstractClasses.Position;

public class MovingObject2Dim extends MovingObject {

	// initial angle
	private double theta;

	private double thetaRad;
	
	public double getTheta() {
		return theta;
	}

	public void setTheta(double theta) {
		this.theta = theta;
		this.thetaRad = theta*Math.PI/180;
	}
	
	public double getThetaRad(){
		return this.thetaRad;
	}


	
	public MovingObject2Dim(String ID, Position startingPoint,
			Position arrivalPoint) {
		super(ID, startingPoint, arrivalPoint);

		// must compute starting trajectory generation rule
		TrajectoryGenratingRule2D genaratingRule = new TrajectoryGenratingRule2D();

		// uses two points to define trajectory generating function
		Function<MovingObject, Position> generationByPointsPair = new Function<MovingObject, Position>() {

			@Override
			public Position apply(MovingObject t) {
				Position2Dims newPosition = new Position2Dims();

				// known positions coords
				Double 	x_s = ((Position2Dims) t.getStartingPoint()).getX(), 
						y_s = ((Position2Dims) t.getStartingPoint()).getY(), 
						x_a = ((Position2Dims) t.getArrivalPoint()).getX(), 
						y_a = ((Position2Dims) t.getArrivalPoint()).getY();

				// new computed positions
				Double x, y;

				x = x_s + (x_a - x_s) * t.getCurrentTick();
				y = y_s + (y_a - y_s) * t.getCurrentTick();

				newPosition.setX(x);
				newPosition.setY(y);
				return newPosition;
			}

		};

		// uses starting point and theta angle to compute trajectory
		Function<MovingObject, Position> generationByAngle = new Function<MovingObject, Position>() {

			@Override
			public Position apply(MovingObject t) {
				Position2Dims newPosition = new Position2Dims();

				//known positions coords
				Double 	x_s = ((Position2Dims) getStartingPoint()).getX(), 
						y_s = ((Position2Dims) getStartingPoint()).getY();

				// new computed positions
				Double x, y;

				x = x_s + Math.cos(thetaRad) * getCurrentTick();
				y = y_s + Math.sin(thetaRad) * getCurrentTick();

				newPosition.setX(x);
				newPosition.setY(y);
				return newPosition;
			}

		};

//		genaratingRule.setRule(generationByPointsPair);
		genaratingRule.setRule(generationByAngle);
		setRule(genaratingRule);
	}


	public Double[] computeAvoidanceThick(MovingObject2Dim toBeAvoided, double delta){
		Double[] solutions = new Double[2];
		double a_x, a_y, b_x, b_y ,c_x, c_y,a, b, c;
		a_x = Math.cos(getThetaRad()) - Math.cos(toBeAvoided.getThetaRad());
		a_x = Math.pow(a_x,2);
		
		b_x =  this.getStartingPoint().getCoord("x") - toBeAvoided.getStartingPoint().getCoord("x");
		b_x *= Math.cos(getThetaRad()) - Math.cos(toBeAvoided.getThetaRad());
		b_x *= 2;
		
		c_x = this.getStartingPoint().getCoord("x") - toBeAvoided.getStartingPoint().getCoord("x");
		c_x = Math.pow(c_x, 2);
		
		a_y = Math.sin(getThetaRad()) - Math.sin(toBeAvoided.getThetaRad());
		a_y = Math.pow(a_y,2);
		
		b_y =  this.getStartingPoint().getCoord("y") - toBeAvoided.getStartingPoint().getCoord("y");
		b_y *= Math.sin(getThetaRad()) - Math.sin(toBeAvoided.getThetaRad());
		b_y *= 2;
		
		c_y = this.getStartingPoint().getCoord("y") - toBeAvoided.getStartingPoint().getCoord("y");
		c_y = Math.pow(c_y, 2);
		
		a = a_x + a_y;
		b = b_x + b_y;
		c = c_x + c_y - Math.pow(delta,2);
		
		Double sqrtD = Math.sqrt(Math.pow(b,2)-(4*a*c));
		
		solutions[0] = - ( b - sqrtD ) / (2*a);
		solutions[1] = - ( b + sqrtD ) / (2*a);		
		return solutions;
	}
}
