package testJava;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import org.math.plot.Plot2DPanel;

import abstractClasses.Position;

public class TestClassByAngleOnlyDistance {
	
	public static void main(String[] args) {
		TestClassByAngleOnlyDistance self = new TestClassByAngleOnlyDistance();
		List<MovingObject2Dim> movingObjects = new ArrayList<MovingObject2Dim>();
		int objectsNum = 2;
		int meetingTime = 50;
		Double[] thetas = new Double[2];
		PrintWriter csvWriter = null;
		thetas[0]= 45.0;
		thetas[1] = 60.0;
		
		while (objectsNum>0){
			movingObjects.add(new MovingObject2Dim("", null, null));
			objectsNum--;
		}
		
		// primo moving object e' libero:
		MovingObject2Dim freeObject = movingObjects.get(0);
		Position2Dims freeObjectStartingPoint = new Position2Dims();
		
		freeObjectStartingPoint.setX(0);
		freeObjectStartingPoint.setY(0);
				
		freeObject.setStartingPoint(freeObjectStartingPoint);
		freeObject.setTheta(thetas[0]);
		freeObject.setID("FreeObject");
		
		freeObject.setTtl(100);
		
		freeObject.setCurrentTick(1);
		
		// creare secondo moving object tale che abbia un traiettoria passante per meetPoint_star:
		MovingObject2Dim slaveObject = movingObjects.get(1);
		Position2Dims slaveObjectStartingPoint = new Position2Dims();
		slaveObjectStartingPoint.setX(0);
		slaveObjectStartingPoint.setY(0);
		slaveObject.setCurrentTick(1);
		slaveObject.setStartingPoint(slaveObjectStartingPoint);
		slaveObject.setID("SlaveObject");
		slaveObject.setTheta(thetas[1]);
		slaveObject.setTtl(100);

		for(MovingObject2Dim m : movingObjects){	
			String CSV = "";
			m.setCurrentTick(0);
			// loop fino al ttl dell'oggetto
			while (m.getCurrentTick()<m.getTtl() ){
				CSV += m.applyRule().toCSV();
				m.setCurrentTick(m.getCurrentTick()+1);
			}
			try {
				csvWriter = new PrintWriter("csv/"+m.getID()+".txt");
				csvWriter.print(CSV);
				csvWriter.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Double[] solutions = slaveObject.computeAvoidanceThick(freeObject, 3	);
		System.out.println("solutions[0] = " + solutions[0]);
		System.out.println("solutions[1] = " + solutions[1]);
		
	}

}
