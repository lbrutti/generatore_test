package abstractClasses;


public abstract class MovingObject {
	private String ID;
	private Position currentPosition;
	private Position pastPosition;
	private Position startingPoint;
	private Position arrivalPoint;
	private TrajectoryGeneratingRule rule;
	
	// next timestamp tick
	private int currentTick;
	
	
	// time to live
	private int ttl;
	
	public MovingObject(String ID, Position startingPoint, Position arrivalPoint){
		setID(ID);
		setStartingPoint(startingPoint);
		setArrivalPoint(arrivalPoint);
		setCurrentPosition(startingPoint);
	}
	
	public String getID() {
		return ID;
	}
	public void setID(String ID) {
		this.ID = ID;
	}
	public Position getCurrentPosition() {
		return currentPosition;
	}
	public void setCurrentPosition(Position currentPosition) {
		this.currentPosition = currentPosition;
	}
	public Position getPastPosition() {
		return pastPosition;
	}
	public void setPastPosition(Position pastPosition) {
		this.pastPosition = pastPosition;
	}
	public Position getStartingPoint() {
		return startingPoint;
	}
	public void setStartingPoint(Position startingPoint) {
		this.startingPoint = startingPoint;
	}
	public Position getArrivalPoint() {
		return arrivalPoint;
	}
	public void setArrivalPoint(Position arrivalPoint) {
		this.arrivalPoint = arrivalPoint;
	}
	public TrajectoryGeneratingRule getRule() {
		return rule;
	}
	public void setRule(TrajectoryGeneratingRule rule) {
		this.rule = rule;
	}

	public int getCurrentTick() {
		return currentTick;
	}

	public void setCurrentTick(int nextTick) {
		this.currentTick = nextTick;
	}
	

	public int getTtl() {
		return ttl;
	}

	public void setTtl(int ttl) {
		this.ttl = ttl;
	}

	public Position applyRule(){
		return getRule().computeNewPosition(this);
	}
}
