package abstractClasses;


import java.util.function.Function;

import abstractClasses.MovingObject;
import abstractClasses.Position;

public abstract class TrajectoryGeneratingRule {

	private Function<MovingObject, Position> rule = new Function<MovingObject, Position>() {
		public Position apply(MovingObject o) {
			return o.getCurrentPosition();
		}
	};

	public void setRule (Function<MovingObject, Position> newRule){
		this.rule = newRule;
	}
	
	public Function<MovingObject, Position> getRule (){
		return this.rule;
	}
	
	public Position computeNewPosition(MovingObject mo){
		return rule.apply(mo);
	}

}
