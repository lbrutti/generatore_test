package abstractClasses;

import java.util.HashMap;
import java.util.Map;

public abstract class Position {
	private Map<String,Double> coords = new HashMap<String, Double>();
	
	public Double getCoord(String k){
		return coords.get(k);
	}
	
	public Double setCoord(String k, Double coord){
		return coords.put(k, coord);
	}

	public String toString(){
		return coords.toString();
	}
	
	public String toCSV(){
		String csv ="";
		for (java.util.Map.Entry<String, Double> coord : this.coords.entrySet()){
			csv += coord.getValue() + ";";
			csv = csv.replaceAll("\\.",",");
		}
		
		csv += "\n";
		csv = csv.replaceAll(";\\n", "\n");
		
		return csv;
	}
}
